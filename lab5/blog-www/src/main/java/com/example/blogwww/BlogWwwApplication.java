package com.example.blogwww;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogWwwApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogWwwApplication.class, args);
    }
}
