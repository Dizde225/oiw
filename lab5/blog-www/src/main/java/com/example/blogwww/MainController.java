package com.example.blogwww;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class MainController {

    @Autowired
    private PostRepository postRepository;

    @GetMapping("/post")
    public Iterable<Post> getAll(){
        return postRepository.findAll();
    }

    @GetMapping("/post/{id}")
    public Post getPost(@PathVariable int id){
        return postRepository.findById(id);
    }

    @PostMapping("/post")
    public String addPost(@RequestBody Post post){
        postRepository.save(post);
        return "saved";
    }

    @PatchMapping("/post/{id}")
    void patchPost(@PathVariable int id, @RequestBody Post post){
        Post temp = postRepository.findById(id);
        if (post.getText() != null){
            temp.setText(post.getText());
        }
        if (post.getTitle() != null){
            temp.setTitle(post.getTitle());
        }
        postRepository.save(temp);
    }

    @DeleteMapping("/post/{id}")
    public String deletePost(@PathVariable int id){
        if (postRepository.findById(id) == null) return "Fail";
        postRepository.delete(postRepository.findById(id));
        return "Sucess";
    }
}
