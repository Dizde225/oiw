var myHeading = document.querySelector('h1');
myHeading.textContent = 'Hello world!';

var test = 'testing';
if (test === 'chocolate') {
  alert('Yay, I love chocolate ice cream!');
} else {
  alert('Test works!');
}
var addition = addTwoNumber(10,12);
alert('My first function ' + addition);

document.querySelector('ul').onclick = function() {
    alert('Ouch! Stop poking me!');
};

function addTwoNumber(num1, num2) {
  var result = num1 + num2;
  return result;
}


document.querySelector('button').onclick = function() {
    var image = document.querySelector('img');
    var mySrc = image.getAttribute("src");
    if (mySrc === "images/logo.png") {
      image.setAttribute("src","images/turtle.jpg");
    }else{
          image.setAttribute("src","images/logo.png");
    }
}
