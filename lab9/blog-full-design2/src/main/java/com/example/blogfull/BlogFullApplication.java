package com.example.blogfull;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogFullApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogFullApplication.class, args);
    }
}
